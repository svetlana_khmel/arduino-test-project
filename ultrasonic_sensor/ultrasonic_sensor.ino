

#define echoPin 7 // Echo Pin
#define trigPin 8 // Trigger Pin
#define LEDPin 13 // Onboard LED
int maximumRange = 200; // Maximum range needed
int minimumRange = 0; // Minimum range needed
long duration, distance; // Duration used to calculate distance


void setup() {
Serial.begin (9600);
pinMode(trigPin, OUTPUT);
pinMode(echoPin, INPUT);
pinMode(LEDPin, OUTPUT); // Use LED indicator (if required) Serial.begin (9600);
}

void loop() {
digitalWrite(trigPin, LOW);
delayMicroseconds(2);
digitalWrite(trigPin, HIGH); 
delayMicroseconds(10); 
digitalWrite(trigPin, LOW); 
duration = pulseIn(echoPin, HIGH);
distance = duration/58.2;

if (distance >= maximumRange || distance <= minimumRange){
  Serial.println("-1");
  digitalWrite(LEDPin, HIGH);
 }else {
  Serial.println(distance);
  digitalWrite(LEDPin, LOW);
  }
    Serial.println(distance);
  digitalWrite(LEDPin, LOW);
  delay(50);
}
