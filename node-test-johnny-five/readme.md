# Foobar

This project shows how to controll arduino uno with nodejs.
(By this article: https://desertbot.io/blog/control-arduino-with-nodejs)

## Installation

Use the package manager [npm](https://pip.pypa.io/en/stable/) to install modules.

```bash
npm i
```

## Usage

```
node index.js

```

In the new terminal window try the following commands:

```$xslt
curl -X GET http://localhost:8000/led/on
curl -X GET http://localhost:8000/led/off
curl -X GET http://localhost:8000/led/blink
curl -X GET http://localhost:8000/led/stop

```

