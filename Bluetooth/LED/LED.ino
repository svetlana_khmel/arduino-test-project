//Smart LED UDEMY Bluetooth
// LED should be insertrd to 13 (long) + grd
// REMOTE CONTROLL sketch is here http://ai2.appinventor.mit.edu/#6158536084422656
//Make the connections as shown in the above image. Don’t connect the RX & TX pins WHILE/BEFORE  uploading the code !!!!
// Enter 0000 OR 1234.

// Similar lesson https://create.arduino.cc/projecthub/SURYATEJA/bluetooth-control-leds-27edbd
void setup() {
  // put your setup code here, to run once:
  pinMode(13, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  
  if (Serial.available()) {
    int z = Serial.read();
    
    if (z == 1) {
        digitalWrite(13, HIGH);
    }
      
    if (z == 2) {
        digitalWrite(13, LOW);  
      }
    }
 }
